package com.practica.agenda

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.Menu
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.gson.Gson
import com.practica.agenda.databinding.ActivityMainBinding
import com.practica.agenda.ui.LogOut
import com.practica.agenda.ui.contactos.ContactosFragment
import com.practica.agenda.ui.contactos.NuevoContactoFragment
import com.practica.agenda.ui.login.Login
import java.sql.SQLException

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)
        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_notas, R.id.nav_contactos, R.id.nav_tareas
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val myIntent = intent // gets the previously created intent

        if (myIntent?.getStringExtra("Acceso") != "Ok") {
            val intent = Intent(this, Login::class.java)
            startActivity(intent)
            finish()
        }else{
            try {
                val dbHelp = dbHelper(baseContext)
                val dbRead = dbHelp.readableDatabase
                val cursor = dbRead.query(
                    dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                    null,             // The array of columns to return (pass null to get all)
                    null,              // The columns for the WHERE clause
                    null,          // The values for the WHERE clause
                    null,             // don't group the rows
                    null,              // don't filter by row groups
                    null               // The sort order
                )

                var emaildb = ""
                var namedb = ""

                with(cursor) {
                    moveToNext()

                    emaildb =
                        getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_EMAIL))
                    namedb =
                        getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_NAME))

                }


                val navigation = findViewById<NavigationView>(R.id.nav_view)
                val x = navigation.getHeaderView(0)
                val name = x.findViewById<TextView>(R.id.nameUsers)
                val email = x.findViewById<TextView>(R.id.email)
//        val imagen = x.findViewById<ImageView>(R.id.imageView)
//        var objGson = Gson()
//
//
//        val bundle = intent.extras
//        val dato = bundle?.getString("datosUsuario")
//
//        val datosUsuario = objGson.fromJson(dato, Login.DatosUsuario::class.java)

                //println(dato)
                name.text = namedb
                email.text = emaildb
            } catch (e: SQLException){
                println("Error mi estimado"+e)
            }
        }



    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.action_settings -> {
                LogOut.logOut(baseContext, this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}