package com.practica.agenda.ui.login

import android.content.ContentValues
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.BaseColumns
import android.widget.EditText
import android.widget.Toast
import com.google.gson.Gson
import com.practica.agenda.MainActivity
import com.practica.agenda.R
import com.practica.agenda.databinding.ActivityLoginBinding
import com.practica.agenda.dbHelper
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException


class Login : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_login)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLogin.setOnClickListener {
            val email = findViewById<EditText>(R.id.email).text.toString()
            val password = findViewById<EditText>(R.id.password).text.toString()

            var url = "http://192.168.1.70:8000/api/login"

            var gJson = Gson()

            val tipoPet = "application/json; charset=utf-8".toMediaType()

            var datosJsonProd = gJson.toJson(DatosLogin(email, password))

            var request = Request.Builder().url(url).post(datosJsonProd.toRequestBody(tipoPet))

            request.addHeader("X-Requested-With", "XMLHttpRequest")
            var client = OkHttpClient()
            client.newCall(request.build()).enqueue(object : Callback {
                override fun onResponse(call: Call, response: Response) {
                    var datos = response?.body?.string()

                    println("Respuesta Login-> " + datos)

                    val respuesta = gJson?.fromJson(datos, DatosWS::class.java)

                    if (respuesta.token == "") {
                        runOnUiThread {
                            Toast.makeText(baseContext,respuesta.message, Toast.LENGTH_SHORT)
                                .show()
                        }
                    } else {

                        runOnUiThread {
                            Toast.makeText(baseContext,respuesta.message, Toast.LENGTH_SHORT)
                                .show()
                        }

                        val dbHelp = dbHelper(baseContext)
                        val dbRead = dbHelp.readableDatabase

                        val projection = arrayOf(
                            BaseColumns._ID,
                            dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_IDUSR,
                            dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_NAME,
                            dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN,
                            dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_EMAIL
                        )


                        val cursor = dbRead.query(
                            dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                            null,             // The array of columns to return (pass null to get all)
                            null,              // The columns for the WHERE clause
                            null,          // The values for the WHERE clause
                            null,             // don't group the rows
                            null,              // don't filter by row groups
                            null              // The sort order
                        )

                        val existeReg: Boolean


                        with(cursor) {
                            existeReg = moveToNext()

                            //if(existeReg)
                            //Toast.makeText(baseContext, getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN)), Toast.LENGTH_SHORT).show()

                        }

                        val db = dbHelp.writableDatabase
                        val values = ContentValues().apply {
                            put(
                                dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_IDUSR,
                                respuesta.userId
                            )
                            put(
                                dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_NAME,
                                respuesta.displayName
                            )
                            put(
                                dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN,
                                respuesta.token
                            )
                            put(
                                dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_EMAIL,
                                respuesta.email
                            )

                        }

                        if (existeReg) {
                            db.update(
                                dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,
                                values,
                                null,
                                null
                            )

                        } else {
                            val newRowId = db?.insert(
                                dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,
                                null,
                                values
                            )
                        }

                        val intent = Intent(baseContext, MainActivity::class.java)
                        intent.putExtra("Acceso", "Ok")
                        startActivity(intent)
                        finish()
                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    println("Error 2 " + e.message.toString())
                }
            })


        }
    }


//
//        val btnLogin = findViewById<Button>(R.id.btn_login)
//        btnLogin.setOnClickListener {
//
//
//
//
//
//            var url = "http://192.168.0.111:8000/api/login"
//
//            var gJson = Gson()
//
//            val mediaType = "application/json; charset=utf-8".toMediaType()
//
//            var jsonSting = gJson.toJson(
//                Login(
//                    password.text.toString(),
//                    email.text.toString(),
//                )
//            )
//
//            var request = Request.Builder().url(url).post(jsonSting.toRequestBody(mediaType))
//
//            val cliente = OkHttpClient()
//
//            cliente.newCall(request.build()).enqueue(object : Callback {
//                override fun onResponse(call: Call, response: Response) {
//                    var txtJson = response?.body?.string()
//                    println(txtJson)
//                    if (txtJson?.length == 2) {
//                        runOnUiThread() {
//                            Toast.makeText(
//                                applicationContext,
//                                "Error de credenciales :(",
//                                Toast.LENGTH_SHORT
//                            ).show()
//                        }
//                    } else {
//                        runOnUiThread() {
//                            val intento1 = Intent(applicationContext, MainActivity::class.java)
//                            var objtGson = Gson()
//                            var datosRequest =
//                                objtGson?.fromJson(txtJson, Array<DatosUsuario>::class.java)
//                            var objJson = Gson()
//                            var datosJson = objJson.toJson(datosRequest[0])
//                            val bundle = bundleOf("datosUsuario" to datosJson)
//                            intento1.putExtra("datosUsuario", datosJson)
//                            println(datosJson)
//                            startActivity(intento1)
//                            Toast.makeText(applicationContext, "Bienvenido", Toast.LENGTH_SHORT)
//                                .show()
//
//                        }
//                    }
//                }
//
//                override fun onFailure(call: Call, e: IOException) {
//                    runOnUiThread() {
//                        Toast.makeText(
//                            applicationContext,
//                            "No hay conexion con el servidor :(",
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    }
//                }
//            })
//
//
//        }
//    }
//
//    class DatosUsuario(
//        val name: String,
//        val email: String,
//        val profile_photo_url: String
//    )
//
//    class Login(
//        var password: String,
//        var email: String
//    )
    data class DatosLogin(
        val email: String,
        val password: String
    )

    data class DatosWS(
        val displayName: String,
        val userId: Int,
        val email: String,
        val token: String,
        val message: String
    )
}