package com.practica.agenda.ui.tareas

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Button
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.practica.agenda.R
import com.practica.agenda.dbHelper
import okhttp3.*
import java.io.IOException

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TareasFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TareasFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.menu_tareas, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var listaTareas = (activity as Activity).findViewById<RecyclerView>(R.id.lista_tareas)
        when (item.itemId) {
            R.id.action_obtener_tareas -> obtenerTareas(listaTareas)
            R.id.action_nueva_tarea->findNavController().navigate(R.id.nav_nueva_tarea)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var vista = inflater.inflate(R.layout.fragment_tareas, container, false)
        var btnTareas = vista.findViewById<Button>(R.id.btn_obtener_tareas)
        var listaTareas = vista.findViewById<RecyclerView>(R.id.lista_tareas)

        btnTareas.setOnClickListener() {
            obtenerTareas(listaTareas)
        }
        listaTareas.layoutManager = LinearLayoutManager(context)
        return vista
    }

    fun obtenerTareas(listaTareas: RecyclerView) {

        Toast.makeText(context, "Obteniendo Tareas", Toast.LENGTH_SHORT).show()
        var act = activity as Activity
        var url = "http://192.168.1.70:8000/api/listar_tareas"

        val dbHelp = dbHelper(context as Context)
        val dbRead = dbHelp.readableDatabase
        val cursor = dbRead.query(
            dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
            null,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,             // don't group the rows
            null,              // don't filter by row groups
            null               // The sort order
        )

        var token = ""

        with(cursor) {
            moveToNext()

            token = getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN))
        }


        var requesst = Request.Builder().url(url)
        requesst.addHeader("Accept","application/json")
        requesst.addHeader("Authorization","Bearer "+token)



        var cliente = OkHttpClient()

        cliente.newCall(requesst.build()).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                var txtJson = response?.body?.string()
                println(txtJson)
                act.runOnUiThread() {
                    var objGson = Gson()
                    var varTareas = objGson?.fromJson(txtJson, Array<Tareas>::class.java)
                    println(varTareas)
                    listaTareas.adapter = TareasAdapter(varTareas)
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                act.runOnUiThread() {
                    Toast.makeText(
                        context,
                        "No se sincronizaron los datos :(",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            }
        })

    }

    class Tareas(
        var id: Int,
        var titulo: String,
        var descripcion: String,
        var prioridad: Int
    )

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TareasFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TareasFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}