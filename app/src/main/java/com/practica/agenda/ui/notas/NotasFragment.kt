package com.practica.agenda.ui.notas
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Button
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.practica.agenda.R
import com.practica.agenda.dbHelper
import okhttp3.*
import java.io.IOException

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [NotasFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NotasFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.menu_notas, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var listanotas = (activity as Activity).findViewById<RecyclerView>(R.id.lista_notas)

        when (item.itemId) {
            R.id.action_obtener_notas -> obtenerNotas(listanotas)
            R.id.action_settings -> Toast.makeText(context, "Menu Settings", Toast.LENGTH_LONG)
                .show()
            R.id.action_nueva_nota->findNavController().navigate(R.id.nav_nueva_nota)


        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var vista = inflater.inflate(R.layout.fragment_notas, container, false)
        var btnNotas = vista.findViewById<Button>(R.id.btn_obtener_notas)
        var listaNotas = vista.findViewById<RecyclerView>(R.id.lista_notas)

        btnNotas.setOnClickListener() {
            obtenerNotas(listaNotas)
        }
        listaNotas.layoutManager = LinearLayoutManager(context)
        return vista
    }

    fun obtenerNotas(listaNotas: RecyclerView) {

        Toast.makeText(context, "Notas", Toast.LENGTH_SHORT).show()
        var act = activity as Activity
        var url = "http://192.168.1.70:8000/api/listar_notas"

        val dbHelp = dbHelper(context as Context)
        val dbRead = dbHelp.readableDatabase
        val cursor = dbRead.query(
            dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
            null,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,             // don't group the rows
            null,              // don't filter by row groups
            null               // The sort order
        )

        var token = ""

        with(cursor) {
            moveToNext()

            token = getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN))
        }


        var requesst = Request.Builder().url(url)
        requesst.addHeader("Accept","application/json")
        requesst.addHeader("Authorization","Bearer "+token)

        var cliente = OkHttpClient()

        cliente.newCall(requesst.build()).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                var txtJson = response?.body?.string()
                println(txtJson)
                act.runOnUiThread() {
                    var objGson = Gson()
                    var varNotas = objGson?.fromJson(txtJson, Array<Notas>::class.java)
                    println(varNotas)
                    listaNotas.adapter = NotasAdapter(varNotas)
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                act.runOnUiThread() {
                    Toast.makeText(
                        context,
                        "Falla al sincronizar los datos",
                        Toast.LENGTH_SHORT
                    )
                        .show()

                }
            }
        })

    }
    class Notas(
        var id: Int,
        var titulo: String,
        var contenido: String

    )

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NotasFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NotasFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
