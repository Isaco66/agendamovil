package com.practica.agenda.ui.notas

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.practica.agenda.R
import com.practica.agenda.dbHelper
import com.practica.agenda.ui.notas.NotasFragment
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [NuevaNotaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NuevaNotaFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.eliminar_nota, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_eliminar_notas -> eliminarNota()
            R.id.action_settings -> Toast.makeText(context, "Menu Settings", Toast.LENGTH_LONG)
                .show()
        }
        return super.onOptionsItemSelected(item)
    }

    fun eliminarNota() {
        var act = activity as Activity
        var objGson = Gson()
        var datosNotas = objGson.fromJson(
            arguments?.getString("Notas"),
            NotasFragment.Notas::class.java
        )
        var id = datosNotas?.id
        if (id != null) {
            var url = "http://192.168.1.70:8000/api/borrar_nota/" + id
            val dbHelp = dbHelper(context as Context)
            val dbRead = dbHelp.readableDatabase
            val cursor = dbRead.query(
                dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                null,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,             // don't group the rows
                null,              // don't filter by row groups
                null               // The sort order
            )

            var token = ""

            with(cursor) {
                moveToNext()
                token = getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN))
            }


//            var requesst = Request.Builder().url(url)
            var requesst = Request.Builder().url(url).delete()
            requesst.addHeader("Accept","application/json")
            requesst.addHeader("Authorization","Bearer "+token)

            var cliente = OkHttpClient()
            cliente.newCall(requesst.build()).enqueue(object : Callback {
                override fun onResponse(call: Call, response: Response) {
                    var txtJson = response?.body?.string()
                    println(txtJson)
                    act.runOnUiThread() {
                        Toast.makeText(context, "Registro eliminado", Toast.LENGTH_LONG).show()
                        findNavController().navigate(R.id.nav_notas)
                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    act.runOnUiThread() {
                        Toast.makeText(
                            context,
                            "Fallo al borrar la nota",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })

        } else {
            Toast.makeText(context, "No hay elemento por eliminar", Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var act = activity as Activity
        var vista = inflater.inflate(R.layout.fragment_nueva_nota, container, false)

        var titulo = vista.findViewById<TextView>(R.id.txt_titulo)
        var contenido = vista.findViewById<TextView>(R.id.txt_contenido)

        var button = vista.findViewById<Button>(R.id.btn_guardar_nota)

        var objGson = Gson()

        var datosNotas = objGson.fromJson(
            arguments?.getString("Notas"),
            NotasFragment.Notas::class.java
        )

        titulo.text = datosNotas?.titulo
        contenido.text = datosNotas?.contenido


        button.setOnClickListener {

            var url = "http://192.168.1.70:8000/api/guardar_nota"

            var gJson = Gson()

            val mediaType = "application/json; charset=utf-8".toMediaType()

            var jsonSting = gJson.toJson(
               Notas(
                    datosNotas?.id,
                    titulo.text.toString(),
                    contenido.text.toString()
                )
            )

            val dbHelp = dbHelper(context as Context)
            val dbRead = dbHelp.readableDatabase
            val cursor = dbRead.query(
                dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                null,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,             // don't group the rows
                null,              // don't filter by row groups
                null               // The sort order
            )

            var token = ""

            with(cursor) {
                moveToNext()

                token = getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN))
            }


            var request = Request.Builder().url(url).post(jsonSting.toRequestBody(mediaType))

            request.addHeader("Accept","application/json")
            request.addHeader("Authorization","Bearer "+token)


            val cliente = OkHttpClient()

            cliente.newCall(request.build()).enqueue(object : Callback {
                override fun onResponse(call: Call, response: Response) {
                    act.runOnUiThread() {
                        Toast.makeText(
                            context,
                            "Nota guardada",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        findNavController().navigate(R.id.nav_notas)
                    }

                }

                override fun onFailure(call: Call, e: IOException) {
                    act.runOnUiThread() {
                        Toast.makeText(
                            context,
                            "No se puede conectar al servidor",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })

        }

        return vista
    }

    class Notas(
        var id: Int?,
        var titulo: String,
        var contenido: String
    )

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NuevaNotaFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NuevaNotaFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}