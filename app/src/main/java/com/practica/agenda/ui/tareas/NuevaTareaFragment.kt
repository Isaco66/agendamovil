package com.practica.agenda.ui.tareas

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.*
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.practica.agenda.R
import com.practica.agenda.dbHelper
import com.practica.agenda.ui.LogOut
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [NuevaTareaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NuevaTareaFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.eliminar_tarea, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var act = activity as Activity
        when (item.itemId) {
            R.id.action_eliminar_tareas -> eliminarTarea()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var act = activity as Activity
        val vista = inflater.inflate(R.layout.fragment_nueva_tarea, container, false)

        val spinner: Spinner = vista.findViewById(R.id.prioridad_spinner)
        var titulo = vista.findViewById<TextView>(R.id.txt_titulo)
        var descripcion = vista.findViewById<TextView>(R.id.txt_descripcion)

        val lista = resources.getStringArray(R.array.prioridad_array)

        val adaptador =
            ArrayAdapter(
                requireContext(),
                android.R.layout.simple_spinner_item,
                lista
            ).also { adaptador ->
                // Specify the layout to use when the list of choices appears
                adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                spinner.adapter = adaptador
            }

        var datos = 0
        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var dato = lista[position].get(0).toString()
                if (!dato.equals("S")) {
                    datos = dato.toInt()
                    println(datos)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }


        var button = vista.findViewById<Button>(R.id.btn_guardar_tarea)


        var objGson = Gson()

        var datosTareas =
            objGson.fromJson(arguments?.getString("Tareas"), TareasFragment.Tareas::class.java)

        titulo.text = datosTareas?.titulo
        descripcion.text = datosTareas?.descripcion
        spinner.setSelection(obtenerPosicionItem(spinner, datosTareas?.prioridad.toString()))
        button.setOnClickListener {
            var url = "http://192.168.1.70:8000/api/guardar_tarea"
            var gJson = Gson()
            val mediaType = "application/json; charset=utf-8".toMediaType()
            var jsonSting = gJson.toJson(
                Tareas(
                    datosTareas?.id,
                    titulo.text.toString(),
                    descripcion.text.toString(),
                    prioridad = datos
                )
            )
            val dbHelp = dbHelper(context as Context)
            val dbRead = dbHelp.readableDatabase
            val cursor = dbRead.query(
                dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                null,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,             // don't group the rows
                null,              // don't filter by row groups
                null               // The sort order
            )
            var token = ""
            with(cursor) {
                moveToNext()
                token =
                    getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN))
            }
            var request = Request.Builder().url(url).post(jsonSting.toRequestBody(mediaType))
            request.addHeader("Accept", "application/json")
            request.addHeader("Authorization", "Bearer " + token)
            val cliente = OkHttpClient()
            cliente.newCall(request.build()).enqueue(object : Callback {
                override fun onResponse(call: Call, response: Response) {
                    act.runOnUiThread() {
                        Toast.makeText(
                            context,
                            "Tarea guardada :)",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        println(jsonSting)
                        findNavController().navigate(R.id.nav_tareas)
                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    act.runOnUiThread() {
                        Toast.makeText(
                            context,
                            "No se puede conectar al servidor",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })
        }
        return vista
    }

    class Tareas(
        var id: Int?,
        var titulo: String,
        var descripcion: String,
        var prioridad: Int
    )

    fun eliminarTarea() {
        var act = activity as Activity
        var objGson = Gson()
        var datosTareas = objGson.fromJson(
            arguments?.getString("Tareas"),
            TareasFragment.Tareas::class.java
        )
        var id = datosTareas?.id
        if (id != null) {
            var url = "http://192.168.1.70:8000/api/borrar_tarea/" + id
            val dbHelp = dbHelper(context as Context)
            val dbRead = dbHelp.readableDatabase
            val cursor = dbRead.query(
                dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                null,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,             // don't group the rows
                null,              // don't filter by row groups
                null               // The sort order
            )
            var token = ""
            with(cursor) {
                moveToNext()
                token =
                    getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN))
            }
            var requesst = Request.Builder().url(url).delete()
            requesst.addHeader("Accept", "application/json")
            requesst.addHeader("Authorization", "Bearer " + token)
            var cliente = OkHttpClient()
            cliente.newCall(requesst.build()).enqueue(object : Callback {
                override fun onResponse(call: Call, response: Response) {
                    var txtJson = response?.body?.string()
                    println(txtJson)
                    act.runOnUiThread() {
                        Toast.makeText(
                            context,
                            "La Tarea se eliminó",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        findNavController().navigate(R.id.nav_tareas)
                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    act.runOnUiThread() {
                        Toast.makeText(
                            context,
                            "No se pudo borrar la Tarea :(",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })

        } else {
            Toast.makeText(context, "No hay elemnto por eliminar", Toast.LENGTH_LONG).show()
        }
    }

    fun obtenerPosicionItem(spinner: Spinner, prioridad: String?): Int {
        //Creamos la variable posicion y lo inicializamos en 0
        var posicion = 0
        //Recorre el spinner en busca del ítem que coincida con el parametro `String prioridad`
        //que lo pasaremos posteriormente
        for (i in 0..spinner.count - 1) {
            //Almacena la posición del ítem que coincida con la búsqueda
            if (spinner.getItemAtPosition(i).toString().get(0) == prioridad?.get(0)) {
                posicion = i
            }
        }
        //Devuelve un valor entero (si encontro una coincidencia devuelve la
        // posición 0 o N, de lo contrario devuelve 0 = posición inicial)
        return posicion
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NuevaTareaFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NuevaTareaFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
