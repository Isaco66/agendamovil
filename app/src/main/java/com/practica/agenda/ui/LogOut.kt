package com.practica.agenda.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.google.gson.Gson
import com.practica.agenda.MainActivity
import com.practica.agenda.dbHelper
import okhttp3.*
import java.io.IOException

class LogOut {

    companion object{
        @JvmStatic
        fun logOut(context:Context, activity: Activity) {
            var x = ""
            //var act = activity as Activity
            var url = "http://192.168.1.70:8000/api/logout"

            val dbHelp = dbHelper(context as Context)
            val dbRead = dbHelp.readableDatabase
            val cursor = dbRead.query(
                dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
                null,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,             // don't group the rows
                null,              // don't filter by row groups
                null               // The sort order
            )

            var token = ""

            with(cursor) {
                moveToNext()

                token =
                    getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN))
            }

//        requesst.addHeader("Accept","application/json")
//        requesst.addHeader("Authorization","Bearer "+token)


            var requesst = Request.Builder().url(url)
            requesst.addHeader("Accept", "application/json")
            requesst.addHeader("Authorization", "Bearer " + token)


            var cliente = OkHttpClient()

            cliente.newCall(requesst.build()).enqueue(object : Callback {
                override fun onResponse(call: Call, response: Response) {
                    var txtJson = response?.body?.string()
                    var objGson = Gson()
                    var datosRequest = objGson?.fromJson(txtJson, Array<Mensaje>::class.java)
                    var objJson = Gson()
                    var datosJson = objJson.toJson(datosRequest[0])

                    val respuesta = objGson.fromJson(datosJson, Mensaje::class.java)
                    activity.runOnUiThread() {
                        if (respuesta.message == "Ok") {

                            Toast.makeText(context, "Sesion Caducada", Toast.LENGTH_SHORT).show()

                        } else {
                            Toast.makeText(
                                context,
                                "Error la sesion ha caducado",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }

                override fun onFailure(call: Call, e: IOException) {
                    activity.runOnUiThread() {
                        Toast.makeText(context, "No hay conexion al servidor", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            })
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra("Acceso", "Adios")
            activity.startActivity(intent)
            activity.finish()
        }
    }
    class Mensaje(val message:String)



}