package com.practica.agenda.ui.tareas

import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.util.Size
import android.util.SizeF
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import com.google.gson.Gson
import com.practica.agenda.R
import com.practica.agenda.ui.contactos.ContactosFragment
import com.practica.agenda.ui.contactos.bundleOf
import java.io.Serializable

class TareasAdapter(val datos: Array<TareasFragment.Tareas>) : RecyclerView.Adapter<CustomView>() {
    override fun getItemCount(): Int {
        return datos.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomView {
        val layoutInflater = LayoutInflater.from(parent?.context)
        val cellForRow = layoutInflater.inflate(R.layout.row_tareas, parent, false)
        return CustomView(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomView, position: Int) {
        holder?.itemView?.setOnClickListener {
            val navController = holder?.itemView.findNavController()

            var objJson = Gson()

            var datosJson = objJson.toJson(datos[position])
            println(position)
            println(datosJson)

            val bundle = bundleOf("Tareas" to datosJson)

            navController.navigate(R.id.nav_nueva_tarea, bundle)
        }



        val titulo = holder.itemView.findViewById(R.id.titulo_tarea) as TextView
        titulo.text=datos[position].titulo
        val descripcion = holder.itemView.findViewById(R.id.descripcion) as TextView
        descripcion.text=datos[position].descripcion
        val prioridad = holder.itemView.findViewById(R.id.prioridad) as TextView
        prioridad.text= datos[position].prioridad.toString()
    }


}

class CustomView(varV: View) : RecyclerView.ViewHolder(varV) {

}