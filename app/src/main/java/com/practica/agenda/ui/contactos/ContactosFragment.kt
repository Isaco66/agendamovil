package com.practica.agenda.ui.contactos

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Button
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.practica.agenda.R
import com.practica.agenda.dbHelper
import com.practica.agenda.ui.LogOut
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import java.io.IOException

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ContactosFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ContactosFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.menu_contactos, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var listaContactos = (activity as Activity).findViewById<RecyclerView>(R.id.lista_contactos)
        when (item.itemId) {
            R.id.action_obtener_contactos -> obtenerContactos(listaContactos)
            R.id.action_agregar_contacto->findNavController().navigate(R.id.nav_nuevo_contacto)
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var vista = inflater.inflate(R.layout.fragment_contactos, container, false)
        var btnContactos = vista.findViewById<Button>(R.id.btn_obtener_contactos)
        var listaContactos = vista.findViewById<RecyclerView>(R.id.lista_contactos)

        btnContactos.setOnClickListener() {
            obtenerContactos(listaContactos)
        }
        listaContactos.layoutManager = LinearLayoutManager(context)
        return vista
    }

    fun obtenerContactos(listaContactos: RecyclerView) {

        Toast.makeText(context, "Obteniendo Contactos", Toast.LENGTH_SHORT).show()
        var act = activity as Activity
        var url = "http://192.168.1.70:8000/api/listar_contactos"

        val dbHelp = dbHelper(context as Context)
        val dbRead = dbHelp.readableDatabase
        val cursor = dbRead.query(
            dbHelper.FeedReaderContract.FeedEntry.TABLE_NAME,   // The table to query
            null,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,             // don't group the rows
            null,              // don't filter by row groups
            null               // The sort order
        )

        var token = ""

        with(cursor) {
            moveToNext()

            token = getString(getColumnIndexOrThrow(dbHelper.FeedReaderContract.FeedEntry.COLUMN_NAME_TOKEN))
        }


        var requesst = Request.Builder().url(url)
        requesst.addHeader("Accept","application/json")
        requesst.addHeader("Authorization","Bearer "+token)



        var cliente = OkHttpClient()

        cliente.newCall(requesst.build()).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                var txtJson = response?.body?.string()
                println(txtJson)
                act.runOnUiThread() {
                    var objGson = Gson()
                    var varContactos = objGson?.fromJson(txtJson, Array<Contactos>::class.java)
                    println(varContactos)
                    listaContactos.adapter = ContactosAdapter(varContactos)
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                act.runOnUiThread() {
                    Toast.makeText(
                        context,
                        "No se sincronizaron los datos :(",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            }
        })

    }

    class Contactos(
        var id: Int,
        var nombre: String,
        var apellidop: String,
        var apellidom: String,
        var telefono: String
    )

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ContactosFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ContactosFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}